const express = require("express");
const bodyParser = require("body-parser");
const { user_game, user_game_biodata, user_game_history } = require("./models");
const app = express();
const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });
const port = "3000";
app.set('view engine', 'ejs');



let user = [];

const verifyAuthToken = (req, res, next) => {
  let auth = req.headers['authorization'];
  let obj = user.find(o => o.token === auth)
  if (obj) {
    res.user = obj;
    next();
  } else {
    res.status(401).json({
      "message": "Unauthorized"
    });
  }
}

//routing register
app.post('/register', jsonParser, (req, res) => {
    let body = req.body;
    let email = body.email;
    let password = body.password;
    let r = (Math.random() + 1).toString(36).substring(7);

    user.push({ email, password, token: r });
    console.log(user);

    res.send({
        message: "Register berhasil"
    });
});

//routing login
app.post('/login', jsonParser, (req, res) => {
    let body = req.body;
    let email = body.email;
    let password = body.password;
    
    let obj = user.find(o => o.email === email)

    if (obj && obj.password === password){
        res.send({
            message : "Login Berhasil",
            token : obj.token
        });
    } else {
        res.send({
            message : "Login Berhasil"
        });
    }
});



app.get("/", (req, res) => {
    user_game.findAll({
        where: {},
        order: [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ],
        include: user_game_biodata, user_game_history
    })
    .then(user_games => {
        res.render('user_games/index', {
            user_games: user_games
        });
    });
});

app.get("/create", (req, res) => {
    res.render('user_games/create');
});

app.get("/edit/:id", (req, res) => {
    user_game.findByPk(req.params.id)
    .then(user_game => {
        res.render('user_games/edit', {
            user_game: user_game
        });
    });
});

app.get("/show/:id", (req, res) => {
    user_game.findByPk(req.params.id, {
        include: user_game_biodata, user_game_history
    })
    .then(user_game => {
        res.render('user_game/show', {
            user_game: user_game
        });
    });
});

app.get("/show/:id/user_game", (req, res) => {
    user_game.findByPk(req.params.id)
    .then(user_game => {
        res.render('user_games/user_game_biodata/create', {
            user_game: user_game
        });
    });
});


app.post("/api/user_game", urlEncoded, async (req, res) => {
    try {
        let user_game = await user_game.create({
            email: req.body.email,
            password: req.body.password
        });

        res.redirect('/');
    } catch (error) {
        res.render('user_games/create', {
            error: error
        });
    }
});

app.post("/api/user_game/edit", urlEncoded, async (req, res) => {
    try {
        let user_game = await user_game.findByPk(req.body.id);
        user_game.email = req.body.email ? req.body.email : user_game.email;
        user_game.password = req.body.password ? req.body.password : user_game.password;
        await user_game.save();

        res.redirect('/');
    } catch (error) {
        res.render('user_games/edit', {
            error: error
        });
    }
});

app.get("/api/user_game/delete/:id", async (req, res) => {
    try {
        const count = await user_game.destroy({
            where: { id: req.params.id }
        });

        if (count > 0) {
            return res.redirect('/');
        } else {
            return res.redirect('/');
        }
    } catch (err) {
        return res.redirect('/');
    }

});

app.post("/api/user_game/user_game_biodata", urlEncoded, async (req, res) => {
    try {
        let user_game_biodata = await user_game_biodata.create({
            user_game_id: parseInt(req.body.user_game_id),
            name_user: req.body.name_user,
            level_user: req.body.level_user,
            address: req.body.address
        });

        res.redirect(`/show/${req.body.user_game_id}`);
    } catch (error) {
        res.render('user_game/user_game_biodata/create', {
            error: error,
            user_game: user_game.findByPk(req.body.user_game_id)
        });
    }
});

app.post("/api/user_game/user_game_history", urlEncoded, async (req, res) => {
    try {
        let user_game_history = await user_game_history.create({
            user_game_id: parseInt(req.body.user_game_id),
            user_game_biodata_id: parseInt(req.body.user_game_biodata_id),
            date_of_match: req.body.date_of_match,
            match_result: req.body.match_result
        });

        res.redirect(`/show/${req.body.user_game_id}`);
    } catch (error) {
        res.render('user_game/user_game_biodata/create', {
            error: error,
            user_game: user_game.findByPk(req.body.user_game_id)
        });
    }
});

app.listen(port, () => {
    console.log(`App listening on port: ${port}`);
})